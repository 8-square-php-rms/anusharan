<?php

if (isset($_POST['submit'])) {
    $std_id = $_POST['std_id'];
    $sub_id = $_POST['term'];

    if ($std_id != '' and $term != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = insert_data($connectionStatus, $std_id, $term);
        if ($status) {
            header('Location: index.php?class_id=success&v=Insertion was successfull');
        } else {
            header('Location: index.php?class_id=error&v=Error:Insertion error');
        }
    } else {
        header('Location: index.php?class_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
