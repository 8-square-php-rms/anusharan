<?php

function connect_db()
{
    $connectionStatus = mysqli_connect('localhost', 'anusharan', 'Anusharan@123', 'crud2');
    if (!$connectionStatus) {
        echo 'Error connecting database.';
        exit;
    }

    return $connectionStatus;
}

function select_data($connectionStatus, $term, $std_id, $name, $std_class)
{
    // $sql = "SELECT students.std_id,students.name,students.roll_no FROM result
    // INNER JOIN  students on result.std_id=students.std_id
    // INNER JOIN course on course.sub_id=result.sub_id
    // INNER JOIN class on result.class_id=class.class_id
    // where result.term='$term' AND result.std_id='$std_id'";

    $sql = "SELECT students.std_id, students.name,students.roll_no,course.course_name,course.full_marks,course.pass_marks,class.std_class,result.term,result.marks_obtain,result.status
    FROM result 
    INNER JOIN students ON result.std_id=students.std_id
    INNER JOIN course ON result.sub_id=course.sub_id
    INNER JOIN class ON result.class_id=class.class_id
    where result.term='$term' AND result.std_id='$std_id'";

    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_num_rows($result) > 0) {
        return $result;
    }

    return false;
}
