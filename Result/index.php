<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>courseIndexPage</title>
    <link rel="stylesheet" type="text/css" media="screen" href="../public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../public/css/Bootstrap/bootstrap.min.css">

</head>
<body>
    <div class="container-fluid">
        <div id="wrapper">
            <div class="row">
                <div class="col-md-4">
                        <div id="left-section">
                            <div class="form-wrapper">
                                <h4>INSERT</h4>
                                <form method="post" action="index.php">
                                    <input type="text" name="std_id" placeholder="Student-Id">
                                    <input type="text" name="name" placeholder="Student-Name">
                                    <input type="text" name="std_class" placeholder="Student-class">
                                    <input type="text" name="term" placeholder="Term">
                                    <button type="submit" name="submit" value="OK">Submit</button>
                                </form>
                            </div> 
                        </div>
                    
                </div>

                <div class="col-md-8">
                    
                        <div id="right-section">
                            <h4>SELECT</h4>
                            <?php
                            include 'db.php';
                            $connectionStatus = connect_db();
                            $term = $_POST['term'];
                            $std_id = $_POST['std_id'];
                            $std_class = $_POST['std_class'];
                            $name = $_POST['name'];
                            if (isset($term)) {
                                $data = select_data($connectionStatus, $term, $std_id, $name, $std_class);

                                echo '<table style="margin-bottom:15px;">';
                                echo '<tr>';
                                echo '<th>Student-Name</th> <th>ID</th> <th>Class</th> <th>Term</th>';
                                echo '<tr>';
                                echo '<td>'."$name";
                                echo '<td>'."$std_id";
                                echo '<td>'."$std_class";
                                echo  '<td>'."$term";
                                echo '</table>';

                                echo '<table>';
                                echo '<tr>';
                                echo '<th>Course-Name</th> <th>Full-Marks</th> <th>Pass-marks</th> <th>Term</th> <th>Marks-Obtained</th> <th>Status</th> ';
                                while ($d = mysqli_fetch_assoc($data)) {
                                    echo '<tr>';
                                    echo '<td>'.$d['course_name'].'</td>';
                                    echo '<td>'.$d['full_marks'].'</td>';
                                    echo '<td>'.$d['pass_marks'].'</td>';
                                    echo '<td>'.$d['term'].'</td>';
                                    echo '<td>'.$d['marks_obtain'].'</td>';
                                    echo '<td>'.$d['status'].'</td>';

                                    $obtained_total = $obtained_total + $d['marks_obtain'];
                                    $total_full = $total_full + $d['full_marks'];
                                    $percentage = (($obtained_total / $total_full) * 100);
                                }
                                echo '</table>';

                                echo 'Obtained Marks:'.' '.$obtained_total;
                                echo'<br>';
                                echo 'Total Marks:'.' '.$total_full;
                                echo'<br>';
                                // echo 'Percentage:'.' '.$percentage.'%';
                                echo 'Precentage:'.number_format((float) $percentage, 2, '.', '').'%';
                            }
                            if (isset($_GET['std_id'])) {
                                echo "<div term='".$_GET['std_id']."'>".$_GET['v'].'</div>';
                            }
                            ?>
    
                        </div>
                        <div class="button">
                        <a href="../index.php" >Home</a>
                        </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>