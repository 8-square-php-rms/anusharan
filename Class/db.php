<?php

function connect_db()
{
    $connectionStatus = mysqli_connect('localhost', 'anusharan', 'Anusharan@123', 'crud2');
    if (!$connectionStatus) {
        echo 'Error connecting database.';
        exit;
    }

    return $connectionStatus;
}

function select_data($connectionStatus)
{
    $sql = 'SELECT * FROM `class`';
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_num_rows($result) > 0) {
        mysqli_close($connectionStatus);

        return $result;
    }

    return false;
}

function insert_data($connectionStatus, $room_no, $std_class, $class_teacher, $class_id)
{
    $sql = "INSERT INTO `class`(`room_no`,`std_class`,`class_teacher`,`class_id`)
    VALUES('$room_no','$class','$class_teacher','$class_id')";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

function update_data($connectionStatus, $room_no, $std_class, $class_teacher, $class_id)
{
    $sql = "UPDATE `class` SET  `room_no`='$room_no',`std_class`='$std_class',`class_teacher`='$class_teacher',`class_id`='$class_id' WHERE `class_id`='$class_id'";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

function delete_data($connectionStatus, $class_id)
{
    $sql = "DELETE FROM `class` WHERE `class_id`='$class_id'";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}
