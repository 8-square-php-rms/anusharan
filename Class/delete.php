<?php

if (isset($_POST['submit'])) {
    $class_id = $_POST['class_id'];

    if ($class_id != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = delete_data($connectionStatus, $class_id);
        if ($status) {
            header('Location: index.php?class_id=success&v=Deletion was successfull');
        } else {
            header('Location: index.php?class_id=error&v=Error:Incorrect Student Id');
        }
    } else {
        header('Location: index.php?class_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
