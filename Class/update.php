<?php

if (isset($_POST['submit'])) {
    $room_no = $_POST['room_no'];
    $class = $_POST['class'];
    $class_teacher = $_POST['class_teacher'];
    $class_id = $_POST['class_id'];

    if ($room_no != '' and $class != '' and $class_teacher != '' and $class_id != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = update_data($connectionStatus, $room_no, $class, $class_teacher, $class_id);
        if ($status) {
            header('Location: index.php?class_id=success&v=Updation was successfull');
        } else {
            header('Location: index.php?class_id=error&v=Error:Incorrect Class Id');
        }
    } else {
        header('Location: index.php?class_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
