<?php

if (isset($_POST['submit'])) {
    $std_id = $_POST['std_id'];
    $sub_id = $_POST['sub_id'];
    $class_id = $_POST['class_id'];
    $term = $_POST['term'];
    $marks_obtain = $_POST['marks_obtain'];
    $status = $_POST['status'];

    if ($std_id != '' and $sub_id != '' and $class_id != '' and $term != '' and $marks_obtain != '' and $status != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = update_data($connectionStatus, $std_id, $sub_id, $class_id, $term, $marks_obtain, $status);
        if ($status) {
            header('Location: index.php?class_id=success&v=Updation was successfull');
        } else {
            header('Location: index.php?class_id=error&v=Error:Incorrect Std_id');
        }
    } else {
        header('Location: index.php?class_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
