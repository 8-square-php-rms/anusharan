<?php

function connect_db()
{
    $connectionStatus = mysqli_connect('localhost', 'anusharan', 'Anusharan@123', 'crud2');
    if (!$connectionStatus) {
        echo 'Error connecting database.';
        exit;
    }

    return $connectionStatus;
}

function select_data($connectionStatus)
{
    $sql = 'SELECT * FROM `result`';

    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_num_rows($result) > 0) {
        mysqli_close($connectionStatus);

        return $result;
    }

    return false;
}

function insert_data($connectionStatus, $std_id, $sub_id, $class_id, $term, $marks_obtain, $status)
{
    $sql = "INSERT INTO `result`(`std_id`,`sub_id`,`class_id`,`term`,`marks_obtain`,`status`)
    VALUES('$std_id','$sub_id','$class_id','$term','$marks_obtain','$status')";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

function update_data($connectionStatus, $std_id, $sub_id, $class_id, $term, $marks_obtain, $status)
{
    $sql = "UPDATE `result` SET  `std_id`='$std_id',`class_id`='$class_id',`term`='$term',`marks_obtain`='$marks_obtain', `status`='status' WHERE `term`='$term'";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

function delete_data($connectionStatus, $term)
{
    $sql = "DELETE FROM `result` WHERE `term`='$term'";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

// $sql = 'SELECT students.name,students.roll_no,course.name,course.full_marks,course.pass_marks,class.class,result.term,result.marks_obtain,result.status
// FROM `result`
// INNER JOIN students ON result.std_id=students.std_id
// INNER JOIN course ON result.sub_id=course.sub_id
// INNER JOIN class ON result.class_id=class.class_id
// WHERE term="$term" and std_id="$stdid"';
