<?php

?>

<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>courseIndexPage</title>
    <link rel="stylesheet" type="text/css" media="screen" href="../public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../public/css/Bootstrap/bootstrap.min.css">

</head>
<body>
    <div class="container-fluid">
        <div id="wrapper">
            <div class="row">
                <div class="col-md-4">
                        <div id="left-section">
                            <div class="form-wrapper">
                                <h4>INSERT</h4>
                                <form method="post" action="insert.php">
                                    <input type="text" name="std_id" placeholder="Student-Id">
                                    <input type="text" name="sub_id" placeholder="Subject-Id">
                                    <input type="text" name="class_id" placeholder="Class-Id">
                                    <input type="text" name="term" placeholder="Term">
                                    <input type="text" name="marks_obtain" placeholder="Obtained-Marks">
                                    <input type="text" name="status" placeholder="Status">
                                    <button type="submit" name="submit" value="OK">Insert</button>
                                </form>
                            </div> 
                            <div class="form-wrapper">
                                <h4>UPDATE</h4>
                                <form method="post" action="update.php">
                                <input type="text" name="std_id" placeholder="Student-Id">
                                    <input type="text" name="sub_id" placeholder="Subject-Id">
                                    <input type="text" name="class_id" placeholder="Class-Id">
                                    <input type="text" name="term" placeholder="Term">
                                    <input type="text" name="marks_obtain" placeholder="Obtained-Marks">
                                    <input type="text" name="status" placeholder="Status">
                                    <button type="submit" name="submit" value="OK">Update</button>
                                </form>
                            </div> 
                            <div class="form-wrapper">
                                <h4>DELETE</h4>
                                <form method="post" action="delete.php">
                                <input type="text" name="term" placeholder="Term">
                                <button type="submit" name="submit" value="OK">Delete</button>
                                </form>
                            </div>  
                        </div>
                    
                </div>

                <div class="col-md-8">
                    
                        <div id="right-section">
                            <h4>SELECT</h4>
                            <?php
                            include 'db.php';
                            $connectionStatus = connect_db();
                            $data = select_data($connectionStatus);

                            echo '<table>';
                            echo '<tr>';
                            echo '<th>Std_id.</th><th>Sub_id</th><th>Class_id</th> <th>Term</th><th>Marks Obtained</th> <th>Status</th> ';
                            while ($d = mysqli_fetch_assoc($data)) {
                                echo '<tr>';
                                echo '<td>'.$d['std_id'].'</td>';
                                echo '<td>'.$d['sub_id'].'</td>';
                                echo '<td>'.$d['class_id'].'</td>';
                                echo '<td>'.$d['term'].'</td>';
                                echo '<td>'.$d['marks_obtain'].'</td>';
                                echo '<td>'.$d['status'].'</td>';
                            }
                            echo '</table>';

                            if (isset($_GET['std_id'])) {
                                echo "<div term='".$_GET['std_id']."'>".$_GET['v'].'</div>';
                            }
                            ?>
    
                        </div>
                        <div class="button">
                        <a href="../index.php" >Home</a>
                        </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>