<!DOCTYPE html>
<html lang="en">

<head>
    <!--Meta tags-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device width, initial-scale=1">
    <title>SRMS</title>

    <!--Roboto condensed font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!--Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="public/css/Bootstrap/bootstrap.min.css">

    <!--External stylesheet-->
    <link rel="stylesheet" type="text/css" href="public/css/style.css">

</head>

<body>
    <div class="header">
        <h1>Student Result Management System </h1>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="section1st">
                    <div class="lists">
                        <h4>Options</h4>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="alllist">
                                        <a href="Students/index.php">Students</a>
                                        <a href="Course/index.php">Course</a>
                                        <a href="Class/index.php">Class</a>
                                        <a href="Marks/index.php">Marks</a>
                                        <a href="Result/index.php">Result</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="section2nd">
                    <div class="formpart">


                     </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <p>&copy; Copyright 2019 Result management</p>
    </div>
</body>