<?php

function connect_db()
{
    $connectionStatus = mysqli_connect('localhost', 'anusharan', 'Anusharan@123', 'crud2');
    if (!$connectionStatus) {
        echo 'Error connecting database.';
        exit;
    }

    return $connectionStatus;
}

function select_data($connectionStatus)
{
    $sql = 'SELECT * FROM `students`';
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_num_rows($result) > 0) {
        mysqli_close($connectionStatus);

        return $result;
    }

    return false;
}

function insert_data($connectionStatus, $name, $roll_no, $address)
{
    $sql = "INSERT INTO `students`(`name`,`roll_no`,`address`)
    VALUES('$name','$roll_no','$address')";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

function update_data($connectionStatus, $std_id, $name, $roll_no, $address)
{
    $sql = "UPDATE `students` SET `name`='$name',`roll_no`='$roll_no',`address`='$address' WHERE `std_id`=$std_id";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}

function delete_data($connectionStatus, $std_id)
{
    $sql = "DELETE FROM `students` WHERE `std_id`=$std_id";
    $result = mysqli_query($connectionStatus, $sql);
    if (mysqli_affected_rows($connectionStatus)) {
        return true;
    }

    return false;
}
