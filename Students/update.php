<?php

if (isset($_POST['submit'])) {
    $std_id = $_POST['std_id'];
    $name = $_POST['name'];
    $roll_no = $_POST['roll_no'];
    $address = $_POST['address'];

    if ($std_id != '' and $name != '' and $roll_no != '' and $address != '') {
        include 'Include/db.php';
        $connectionStatus = connect_db();
        $status = update_data($connectionStatus, $std_id, $name, $roll_no, $address);
        if ($status) {
            header('Location: index.php?std_id=success&v=Updation was successfull');
        } else {
            header('Location: index.php?std_id=error&v=Error:Incorrect Student Id');
        }
    } else {
        header('Location: index.php?std_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
