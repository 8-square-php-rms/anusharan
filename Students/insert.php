<?php

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $roll_no = $_POST['roll_no'];
    $address = $_POST['address'];

    if ($name != '' and $roll_no != '' and $address != '') {
        include 'Include/db.php';
        $connectionStatus = connect_db();
        $status = insert_data($connectionStatus, $name, $roll_no, $address);
        if ($status) {
            header('Location: index.php?std_id=success&v=Insertion was successfull');
        } else {
            header('Location: index.php?std_id=error&v=Error:Insertion error');
        }
    } else {
        header('Location: index.php?std_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
