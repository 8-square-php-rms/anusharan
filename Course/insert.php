<?php

if (isset($_POST['submit'])) {
    $name = $_POST['course_name'];
    $full_marks = $_POST['full_marks'];
    $pass_marks = $_POST['pass_marks'];
    $sub_id = $_POST['sub_id'];

    if ($course_name != '' and $full_marks != '' and $pass_marks != '' and $sub_id != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = insert_data($connectionStatus, $course_name, $full_marks, $pass_marks, $sub_id);
        if ($status) {
            header('Location: index.php?sub_id=success&v=Insertion was successfull');
        } else {
            header('Location: index.php?sub_id=error&v=Error:Insertion error');
        }
    } else {
        header('Location: index.php?sub_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
