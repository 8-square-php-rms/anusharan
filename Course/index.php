<?php

?>

<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>courseIndexPage</title>
    <link rel="stylesheet" type="text/css" media="screen" href="../public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../public/css/Bootstrap/bootstrap.min.css">

</head>
<body>
    <div class="container-fluid">
        <div id="wrapper">
            <div class="row">
                <div class="col-md-4">
                        <div id="left-section">
                            <div class="form-wrapper">
                                <h4>INSERT</h4>
                                <form method="post" action="insert.php">
                                    
                                    <input type="text" name="course_name" placeholder="Subject Name">
                                    <input type="text" name="full_marks" placeholder="Full-marks">
                                    <input type="text" name="pass_marks" placeholder="Pass-marks">
                                    <input type="text" name="sub_id" placeholder="Subject_id">
                                    <button type="submit" name="submit" value="OK">Insert</button>
                                </form>
                            </div> 
                            <div class="form-wrapper">
                                <h4>UPDATE</h4>
                                <form method="post" action="update.php">
                                    
                                    <input type="text" name="course_name" placeholder="Subject Name">
                                    <input type="text" name="full_marks" placeholder="Full-marks">
                                    <input type="text" name="pass_marks" placeholder="Pass-marks">
                                    <input type="text" name="sub_id" placeholder="Subject-id">
                                    <button type="submit" name="submit" value="OK">Update</button>
                                </form>
                            </div> 
                            <div class="form-wrapper">
                                <h4>DELETE</h4>
                                <form method="post" action="delete.php">
                                <input type="text" name="sub_id" placeholder="Subject-id">
                                <button type="submit" name="submit" value="OK">Delete</button>
                                </form>
                            </div>  
                        </div>
                    
                </div>

                <div class="col-md-8">
                    
                        <div id="right-section">
                            <h4>SELECT</h4>
                            <?php
                            include 'db.php';
                            $connectionStatus = connect_db();
                            $data = select_data($connectionStatus);

                            echo '<table>';
                            echo '<tr>';
                            echo '<th>Course-Name</th> <th>Full-Marks</th> <th>Pass-Marks</th><th>Subject-Id</th> ';
                            while ($d = mysqli_fetch_assoc($data)) {
                                echo '<tr>';
                                echo '<td>'.$d['course_name'].'</td>';
                                echo '<td>'.$d['full_marks'].'</td>';
                                echo '<td>'.$d['pass_marks'].'</td>';
                                echo '<td>'.$d['sub_id'].'</td>';
                            }
                            echo '</table>';

                            if (isset($_GET['sub_id'])) {
                                echo "<div sub_id='".$_GET['sub_id']."'>".$_GET['v'].'</div>';
                            }
                            ?>
    
                        </div>
                        <div class="button">
                        <a href="../index.php" >Home</a>
                        </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>