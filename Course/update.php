<?php

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $full_marks = $_POST['full_marks'];
    $pass_marks = $_POST['pass_marks'];
    $sub_id = $_POST['sub_id'];

    if ($name != '' and $full_marks != '' and $pass_marks != '' and $sub_id != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = update_data($connectionStatus, $name, $full_marks, $pass_marks, $sub_id);
        if ($status) {
            header('Location: index.php?sub_id=success&v=Updation was successfull');
        } else {
            header('Location: index.php?sub_id=error&v=Error:Incorrect Student Id');
        }
    } else {
        header('Location: index.php?sub_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
