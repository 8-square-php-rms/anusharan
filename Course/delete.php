<?php

if (isset($_POST['submit'])) {
    $sub_id = $_POST['sub_id'];

    if ($sub_id != '') {
        include 'db.php';
        $connectionStatus = connect_db();
        $status = delete_data($connectionStatus, $sub_id);
        if ($status) {
            header('Location: index.php?sub_id=success&v=Deletion was successfull');
        } else {
            header('Location: index.php?sub_id=error&v=Error:Incorrect Student Id');
        }
    } else {
        header('Location: index.php?sub_id=error&v=Error:All fields required');
    }
} else {
    header('Location:index.php');
}
